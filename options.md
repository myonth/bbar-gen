This is a list of beatbar options you can change. To change them, open up the _bbar-gen_ script in a text editor. These options should be in the beginning of the file.

# Pulse options
 `pulse_yn` enables or disables the pulse.

`pulse_type` determines the type of the pulse. There are 3 options:
- `expand` which just expands the tick on pulse;
- `color` which changes tick colors on pulse;
- and `color_expand` which does both.

`pulse_speed` determines the speed, or the duration, of the pulse (not really) in ms.

**Options that only apply to `pulse_type="expand"` or `pulse_type="color_expand"`**

`pulse_expand` is a percentage of the size of the original when pulsing. set to over 100 to expand and less than 100 to shrink.

**Options that only apply to `pulse_type="color"` or `pulse_type="color_expand"`**

`pulse_peak_primcolor` and `pulse_peak_bordcolor` set the primary (fill) and border color when the pulse reaches peak. Uses a weird hex format with no alpha and a weird order, see notes at the bottom of the guide.
`pulse_peak_primalpha` and `pulse_peak_bordalpha` set the alpha for primary and border color. Uses AA hex format, where 00 is solid and FF is opaque. For info about hex see notes.
 
 **Example**
 <code>
 pulse_yn="yes"
 pulse_type="color_expand"
 pulse_speed="64"
 pulse_expand="115"
 pulse_peak_primcolor="BA43FF"
 pulse_peak_bordcolor="FFFFFF"
 pulse_peak_primalpha="00"
 pulse_peak_bordalpha="00"
 </code>

 Assuming I have `tick_border_color="AAFFFFFF"` (somewhat opaque white) and `tick_primary_color="00BA43FF"` (solid pinkish), pulsing will expand the tick while making the border opaque, but won't change the primary color, since that stays the same.

 # Beat and tick options
 `beat_length` is the amount of time it takes for the beat to reach the tick in seconds. This will determine how much silence you should add before the song start.
 
 `font_family` is the font family name (**not** the file name) for the font you created/borrowed to use with the script.

 `font_size_tick` and `font_size_beat` determine the size of the tick and the beats respectively in px(?).

 `font_align` will be 5 by default, which means that the icons (letters) will be aligned to the centre. set to 2 if you want to align to the bottom.

 `tick_border` and `beat_border` determine the thickness of the border. Float values are fine as well.

 `tick_shadow` and `beat_shadow` determine the size of the shadow for the tick and the beat respectively, but I haven't played with shadows yet. Room for exploration.

 `beat_primary_color`, `beat_border_color` and `beat_shadow_color` set the color of the beat primary, border and shadow color respectively. Uses weird hex format with alpha and weird order, see notes.
 
 `tick_primary_color`, `tick_border_color` and `tick_shadow_color` are the same as the above, but for the tick.

 `tick_x` and `tick_y` determine the coordinates of the tick. No matter how you move it, the beats will still come straight from the right side (unless I missed something).

 `tick_fadein` and `tick_fadeout` are the amount of time it takes for the tick to fade in/out in ms. Value of 1000 will fade in/out for 1 second.

 If `tick_devour` is set to "no", the tick will be displayed under the beats. If set to "yes", the opposite is true.

 `overlap` is set to "taiko" by default, meaning earlier beats will overlap the later beats, similar to Taiko no Tatsujin. If you want to change the order, set to "no".

 **Example**
<code>
beat_length="2.1"
font_family="threedxish"
font_size_tick="50"
font_size_beat="38"
font_align="5"
tick_border="5"
beat_border="5"
tick_shadow="0"
beat_shadow="0"
beat_primary_color="00BA43FF"
beat_border_color="00FFFFFF"
tick_primary_color="00BA43FF"
tick_border_color="00FFFFFF"
tick_x="960"
tick_y="1020"
tick_fadein="500"
tick_fadeout="1700"
tick_devour="yes"
overlap="taiko"
</code>

This is a beatbar in which beats move for 2.1 seconds before reaching the tick. The tick is bigger than the beat, and the tick and the beat are both taken from the font called "threedxish".
It aligns everything to the center. Both tick and beats have the border thickness of 5. Shadows are disabled, so not worrying about shadow colors. Beat and tick have identical colors.
Tick is located at the bottom of the screen in the middle. It fades in for 0.5 seconds and fades out in 1.7. The tick overlaps the beats. When beats overlap, they overlap like in taiko.

# Ring options

`ring_yn` enables or disables the ring. Ring starts to expand when the beat reaches the tick.

`ring_length` is the amount of time in seconds during which the ring is visible.

`ring_primary_color`, `ring_border_color` and `ring_shadow_color` are the primary, border and shadow colors respectively. Uses weird hex format with alpha and weird order, see notes.

`ring_border` is the thickness of the ring border. Float values are ok.

`ring_shadow` is the thickness of the shadow.

`ring_expand` is a percentage of the size of the tick when pulsing. set to over 100 to expand and less than 100 to shrink (but why would you shrink?).

`ring_expand_accel` is the acceleration value for ring expansion. See notes for info on acceleration values.

**Example**
<code>
ring_yn="yes"
ring_length="0.35"
ring_primary_color="FFFFFFFF"
ring_border_color="00CB67FF"
ring_border="2"
ring_shadow="0"
ring_expand="200"
ring_expand_accel="0.5"
</code>

The ring will reach 200% size in 0.35 seconds. Due to acceleration, the animation will start out fast and end slow. Shadow disabled. Primary color is fully opaque.
The border is pinkish and thin.

# Afterbeat options

`afterbeat_yn` enables or disables Afterbeat. This will allow for beats to not dissapear on the tick and keep moving in the same direction with the same speed (or so it appears).

`afterbeat_type` determines what happens to the beat past the tick. There are 3 available options:
- `fade` which will make the beat fade out;
- `color_change` which will make the beat change colors;
- and `none` which will leave the beat untouched.

**Options that only apply to `afterbeat_type="fade"`**

`afterbeat_fade_start` and `afterbeat_fade_end` determine when will the fadeout start and end compared to the point when the beat reaches the tick. Value in ms.

`afterbeat_fade_accel` is the acceleration value for fadeout. See notes for info about acceleration values.

**Options that only apply to `afterbeat_type="color_change"`**

`afterbeat_colorc_start` and `afterbeat_colorc_end` determine when will the color change start and end compared to the point when the beat reaches the tick. Value in ms.

`afterbeat_colorc_accel` is the acceleration value for color change. See notes for info about acceleration values.

`afterbeat_colorc_endprimcolor` and `afterbeat_colorc_endbordcolor` are the values for the primary and border colors of the beat after the color change has taken place. Uses weird hex format with no alpha and a weird order, see notes.

**Example 1**
<code>
afterbeat_yn="yes"
afterbeat_type="fade"
afterbeat_fade_start="500"
afterbeat_fade_end="1000"
afterbeat_fade_accel="1"
</code>

After reaching the tick, the beat will continue moving for 0.5 seconds, after which it starts to linearly fade. It is completely gone after 1 second of reaching the tick.

**Example 2**
<code>
afterbeat_yn="yes"
afterbeat_type="color_change"
afterbeat_colorc_start="0"
afterbeat_colorc_end="10"
afterbeat_colorc_accel="0.1"
afterbeat_colorc_endprimcolor="FFFFFF"
afterbeat_colorc_endbordcolor="FFFFFF"
</code>

After reaching the tick, the beat will change it's color to fully white in 0.01 of a second. The color change will start out really fast and end slow, but the timeframe for the change is too small to notice.

# Notes 

- **About the color hex values**

Subtitles use some uncommon format for it's hex value. That is `AABBGGRR`, where AA is alpha, and BBGGRR are the blue, green, and red **in that order**. 

Say you found an RRGGBB value for the color you would like to use. For instance, 12F4AA. You will need to switch "12" and "AA" places in order to use it for the script.

For colors that don't have alpha, the format is `BBGGRR`, of course.

For standalone alpha values, the format is just `AA`.

You might want to use a [converter](https://www.binaryhexconverter.com/decimal-to-hex-converter) to convert decimal values of 0-255 to hex values.

- **Acceleration values**

The acceleration value will change the speed of the transformation during the transformation. In other words, non-linear speed, I think.

The value between 0 and 1 will make the transformation start fast and end slow. Value that's more than 1 will make the transformation start slow and end fast.

If you want it to be linear, set the value to 1.
