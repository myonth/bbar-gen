# Intro
This guide tells how to create custom fonts to use with the script.

You will need a program called [Fontforge](https://fontforge.org/en-US/) and optionally Inkscape if you plan to do your own images.

# Getting an SVG
First of all, you need to get an SVG image to put into your font. You can either find the SVG images on the internet or make it yourself using Inkscape or other vector-based image editing program.

You will need two images: one is for the beat, and the other is for the tick. Note that colors in both don't matter.

If you're using Inkscape, go to _File -> Document Properties and set the page size to 1000x1000 px. You might want to set it as default, too. 

When you created or downloaded your images, you can proceed.

# Generating the font
Open up Fontforge. Select "New".

The script uses the letter "b" for beat and letter "t' for tick. So, double-click the letter "b", and in the new window that opens up go to _File -> Import_, set the format to SVG and select your beat image. Ignore any errors in case they appear. The image should appear in both windows. You can close the newest window and repeat the process with "t" for tick.

Now go to _Element -> Font Info..._ and enter font name and font family name.

- Font name is how the font file will be called.

- Font family is the name that's going to be used by the script. You should remember it.

Now you can export the font. Go to _File -> Generate Fonts..._, select TrueType in a list, and export as [myfont].ttf. Ignore errors such as "Em-Size which is a power of 2" or "This font contains errors". The font is now done.

# Saving the font
This is useful if you're going to preview the beatbar in Aegisub, but not sure whether this is absolutely necessary when you're going to include the font in the .mkv file anyway. At any rate...

- On Linux, type `sudo cp myfont.ttf /usr/share/fonts` to copy your fonts to a system font folder and then `(sudo) fc-cache -f` to update the cache. It is ready to be used.

- On Windows, I found a [guide](https://www.laptopmag.com/articles/install-manage-fonts-windows-10) that explains the process.

# Using the font with the subs
In order to use your font with the subtitle generator, open the _bbar-gen_ script in a text editor and change the value of `font_family` to the font family name you specified in Fontforge. Then run the script.
