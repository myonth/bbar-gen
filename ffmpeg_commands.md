A list of ffmpeg commands for working with subs and fonts. Might come in handy.

**Don't forget to change the names of the variables that are written in uppercase!**

**Also, the output file format will be specified when needed.**
 
# To include subs and font with the video in an mkv
`ffmpeg -i VIDEO -i BEATBAR.ass -attach FONT.ttf -map 0 -map 1 -metadata:s:t mimetype=application/x-trutype-font -c copy OUTPUT.mkv`

# To hardsub when the subtitles are in the mkv file
If you have the font installed on the system:

`ffmpeg -i INPUT.mkv -vf subtitles=INPUT.mkv OUTPUT`

If you don't have the font installed on the system:

`ffmpeg -i INPUT.mkv -vf subtitles=INPUT.mkv:fontsdir=FONTDIR OUTPUT`

where _FONTDIR_ is the directory in which the font is located. `.`(current directory) works as well (at least in bash).

# To hardsub when the subtitles and video are separate
(use the same trick as above if the font isn't installed on the system)

`ffmpeg -i INPUT -vf subtitles=BEATBAR.ass OUTPUT`

# To extract font from the mkv file
`ffmpeg -dump_attachment:t "" -i INPUT.mkv`

# To remove soft subs from the mkv file
`ffmpeg -i INPUT.mkv -map 0 -map -0:s -c copy OUTPUT.mkv`

# To reencode the video to 60 fps 
This duplicates the frames, so theoretically it will still look fine, but subtitles appear much smoother. They will also appear smoother in hardsubs if you convert to 60 fps before hardsubbing.

`ffmpeg -i INPUT -r 60 -c:a copy OUTPUT`

# Generate a video using a solid color
The example generates a 60 fps 1920x1080 video that is 2 m 49 sec long (169 seconds in total) and contains just a green background for you to put subs over.

`ffmpeg -f lavfi -i color=size=1920x1080:rate=60:color=green -t 169 OUTPUT`

# Put audio over a video (will be useful when working with generated color background)

`ffmpeg -i VIDEO -i AUDIO -c:v copy OUTPUT`
