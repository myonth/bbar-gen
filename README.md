# bbar-gen

This bash script generates a beatbar in a form of a subtitle file, using an exported Audacity label track file as input. 

Moreover, this repository contains guides on how to fully customize and use the subtitle file.

Dependencies: **bash**, sed, printf, date, echo, read, tail, bc, rm. In addition, to fully utilize the subtitles, you will need ffmpeg, Fontforge, Inkscape, Audacity and Aegisub.

While linux usually comes with these coreutils and bash by default, Windows users will have to install [Cygwin](https://www.cygwin.com/) or use a virtual machine/WSL.

# Usage
Prior to using the script, take a look at the [options](https://gitlab.com/myonth/bbar-gen/-/blob/master/options.md). You will most likely want to change them.

`./bbar-gen [TRACK-FILE] (OUTPUT-NAME)`

Output name by default is _beatbar.ass_.

# Custom fonts (images)

A guide to create your own beatbar images using vector graphics for usage with the subs is [here](https://gitlab.com/myonth/bbar-gen/-/blob/master/creating_fonts.md).

# Instructions to generate a label track file

- First of all, you have to put beat sounds over your selected song. I used LMMS for this.

- Then, export the beat track _audio_ file. It should be only drums and nothing else. Import it into Audacity.

- Then, in Audacity, select _Analyze > Beat Finder_. Don't set the threshold too high, otherwise it might add labels where there is no beat. Press OK.

- Check the label track quickly to see if any beats are missing, just in case.

- Go to _File > Export > Export Labels_ and export. 

- Then, in the bash shell, run the script like in the usage example. Of course, that example assumes that you've cd'd into the directory containing the script.

- If you get "Permission denied" error, use `chmod +x bbar-gen` to get execute permissions.

**NOTE: the subtitles have a bit of a delay to them. In order to compensate for that, you should add enough silence to the beginning of the track you're going to use, so that the first drum is hit when the fist beat (in the subtitle) ends (dissappears on the tick mark).**

It's not a bad idea to check the subtitle in a program like [Aegisub](https://github.com/Aegisub/Aegisub/releases). This program will also give you an ability to add additional effects, lines, etc. into the subtitle.
# Using subtitles with video

I've made a [list](https://gitlab.com/myonth/bbar-gen/-/blob/master/ffmpeg_commands.md) of ffmpeg commands that can be used to manipulate subtitles and video.

# Example of subtitle with the blank video and music
[A link to the .zip file](https://files.catbox.moe/gbqpbt.zip)


_I am not a programmer, I just know a little bash._
_Any suggestions are appreciated._

_You can contact me through a [matrix room](https://matrix.to/#/#bbargen:matrix.org?via=matrix.org) or directly at @myonth:matrix.org._
